package com.paic.arch.jmsbroker;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.Destination;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

import javax.jms.*;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * The implementation class for Apache ActiveMQ
 *
 * Created by benjamin on 04/04/2018.
 */
public class JmsBrokerServiceActiveMQImpl implements JmsBrokerService {

    private static final Logger LOG = getLogger(JmsBrokerServiceActiveMQImpl.class);

    private String brokerUrl;

    private Connection connection;

    private Session session;

    private BrokerService brokerService;


    /**
     * The constructor
     * @param brokerUrl
     */
    public JmsBrokerServiceActiveMQImpl(String brokerUrl) {
        this.brokerUrl = brokerUrl;
        this.brokerService = new BrokerService();
    }

    /**
     * Create MQ connection and session
     */
    private void initConnection() {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
            connection = connectionFactory.createConnection();
            connection.start();
        } catch (Exception e) {
            LOG.error("Failed to create connection with url: "  + brokerUrl);
        }

        try {
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        } catch (Exception e) {
            LOG.error("Failed to create session.");
        }
    }

    /**
     * Close MQ connection and session
     */
    private void closeConnection() {
        try {
            if (session != null) {
                session.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            LOG.error("Failed to close connection.");
        }
    }


    /**
     * Start the broker service
     */
    @Override
    public void startBroker() {
        try {
            brokerService.start();
        } catch (Exception e) {
            LOG.error("Failed to start broker.");
        }

    }

    /**
     * Stop the broker service
     */
    @Override
    public void stopBroker() {
        try {
            brokerService.stop();
        } catch (Exception e) {
            LOG.error("Failed to stop broker.");
        }
    }

    /**
     * Bind a URL to the broker service
     * @param brokerUrl
     */
    @Override
    public void bindBrokerUrl(String brokerUrl) {
        try {
            brokerService.addConnector(brokerUrl);
            brokerService.setPersistent(false);
        } catch (Exception e) {
            LOG.error("Failed to bind broker with url: "  + brokerUrl);
        }
    }

    /**
     * Return the broker URL
     * @return the broker URL
     */
    @Override
    public String getBrokerUrl() {
        return brokerUrl;
    }

    /**
     * Create a connection and session, then send the specific message to queue
     * @param queueName
     * @param message
     */
    @Override
    public void sendTextMessageToQueue(String queueName, String message) {

        initConnection();

        try {
            Queue queue = session.createQueue(queueName);
            MessageProducer producer = session.createProducer(queue);
            producer.send(session.createTextMessage(message));
            producer.close();
        } catch (Exception e) {
            LOG.error("Failed to send message to queue: " + queueName);
        } finally {
            closeConnection();
        }

    }

    /**
     * Create a connection and session, then retrieve a message from queue
     * @param queueName
     * @param timeOut
     * @return
     */
    @Override
    public String retrieveASingleMessageFromQueue(String queueName, long timeOut) {

        initConnection();

        String receivedMessage = null;

        try {
            Queue queue = session.createQueue(queueName);
            MessageConsumer consumer = session.createConsumer(queue);
            Message message = consumer.receive(timeOut);
            consumer.close();

            if (message != null) {
                receivedMessage = ((TextMessage) message).getText();
            } else {
                LOG.error("No message received from queue: " + queueName);
                throw new NoMessageReceivedException("No message received from queue: " + queueName);
            }

        } catch (JMSException e) {
            LOG.error("Failed to retrieve message from queue: " + queueName);
        } finally {
            closeConnection();
        }

        return receivedMessage;
    }

    /**
     * Get the total number of messages existing on a queue
     * @param queueName
     * @return the total number of messages
     */
    @Override
    public long getMessageCountFromQueue(String queueName) {
        Broker regionBroker = brokerService.getRegionBroker();
        DestinationStatistics destinationStatistics = null;

        for (Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(queueName)) {
                destinationStatistics = destination.getDestinationStatistics();
                break;
            }
        }

        return destinationStatistics.getMessages().getCount();
    }

    /**
     * Check if the queue is empty
     * @param queueName
     * @return true if queue is empty, false if not
     */
    @Override
    public boolean isEmptyQueue(String queueName) {
        long count = 0;

        try {
            count = getMessageCountFromQueue(queueName);
        } catch (Exception e) {
            LOG.error("Failed to get message count from queue: "  + queueName);
        }

        return count >= 0 ? false : true;
    }
}
