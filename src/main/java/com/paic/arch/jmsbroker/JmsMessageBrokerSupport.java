package com.paic.arch.jmsbroker;

import org.slf4j.Logger;

import java.lang.IllegalStateException;

import static org.slf4j.LoggerFactory.getLogger;
import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;

public class JmsMessageBrokerSupport {
    private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);
    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    private String brokerUrl;
    private JmsBrokerService brokerService;

    private JmsMessageBrokerSupport(String aBrokerUrl) {
        brokerUrl = aBrokerUrl;
        brokerService = new JmsBrokerServiceActiveMQImpl(aBrokerUrl);
    }

    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
    }

    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
        JmsMessageBrokerSupport brokerSupport = bindToBrokerAtUrl(aBrokerUrl);
        brokerSupport.createEmbeddedBroker();
        brokerSupport.startEmbeddedBroker();
        return brokerSupport;
    }

    private void createEmbeddedBroker() throws Exception {
        brokerService.bindBrokerUrl(brokerUrl);
    }

    public static JmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
        return new JmsMessageBrokerSupport(aBrokerUrl);
    }

    private void startEmbeddedBroker() throws Exception {
        brokerService.startBroker();
    }

    public void stopTheRunningBroker() throws Exception {
        if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stopBroker();
    }

    public final JmsMessageBrokerSupport andThen() {
        return this;
    }

    public final String getBrokerUrl() {
        return brokerUrl;
    }

    public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) {
        brokerService.sendTextMessageToQueue(aDestinationName, aMessageToSend);
        return this;
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        return brokerService.retrieveASingleMessageFromQueue(aDestinationName, aTimeout);
    }

    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return brokerService.getMessageCountFromQueue(aDestinationName);
    }

    public boolean isEmptyQueueAt(String aDestinationName) throws Exception {
        return brokerService.isEmptyQueue(aDestinationName);
    }

}
