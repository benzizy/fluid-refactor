package com.paic.arch.jmsbroker;

/**
 * Created by benjamin on 08/04/2018.
 */
public class NoMessageReceivedException extends RuntimeException {

    public NoMessageReceivedException(String reason) {
        super(reason);
    }

}
