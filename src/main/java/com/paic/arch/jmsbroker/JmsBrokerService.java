package com.paic.arch.jmsbroker;


import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Session;

/**
 * Created by benjamin on 04/04/2018.
 */
public interface JmsBrokerService {


    void startBroker();

    void stopBroker();

    void bindBrokerUrl(String brokerUrl);

    String getBrokerUrl();

    void sendTextMessageToQueue(String queueName, String message);

    String retrieveASingleMessageFromQueue(String queueName, long timeOut);

    long getMessageCountFromQueue(String queueName);

    boolean isEmptyQueue(String queueName);
}
